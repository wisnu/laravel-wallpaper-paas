<?php
use Illuminate\Support\Facades\Storage;

/*
Route::get('/change', [
    'as' => 'start',
    'uses' => 'Controller@change',
    'middleware' => 'auth.very_basic' 
]);
*/

Route::get('/change', 'Controller@change');


Route::get('change/{niche}', function ($niche) {
// 			$content = Storage::disk('local')->get($niche);
			$content = file_get_contents("http://pool.wis.nu/keywords/$niche");
			Storage::disk('local')->put('keywords.txt', $content);
	    return redirect('/');
});

Route::get('/{page?}', 'Controller@index')->name('home')->where('page', '[1-9]+[0-9]*');
Route::get('{slug}/{page?}', 'Controller@single')->where('page', '[1-9]+[0-9]*');
Route::get('page/{slug}.html', 'Controller@page');
Route::get('{slug}/{permalink}.html', 'Controller@attachment');


