@extends('main')
@section('title')
	{{ ucwords(Config::get('money.niche')) }}
@endsection

@section('meta')
<meta name="description" content="Download {{ ucwords(Config::get('money.niche')) }}">
<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
@endsection


@section('content')
<div class="content">
	<div class="article">
		<header class="main-header">
			<div id="header">
				<a href="{{ url('/') }}" rel="nofollow" title="Resume Example">
				<h1>{{ $_SERVER['HTTP_HOST'] }}</h1></a>
			</div>
		</header>


		<div class="header-text">
			<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<span typeof="v:Breadcrumb"><a href="{{ url('/') }}" property="v:title" rel="v:url">Home</a></span>
			</div>
		</div>

		<div>
			<?=$money['responsiveAds']; //Ads ?>	
		</div>

		<ul>
		
			@foreach ($result as $data)
			<li>
				<a alt="{{ ucwords(str_replace('-', ' ', $data)) }}" href="{{ url('/'.str_slug($data)) }}" title="{{ ucwords(str_replace('-', ' ', $data)) }}">{{ substr(ucwords(str_replace('-', ' ', $data)), 0, 30) }}</a>
			</li>

			@endforeach

		</ul>
		<div class="clear"></div>
		<div id="pagination"><a href="{{ url('/') }}" title="First page">« First</a>...<b>1</b><a href="/2" title="Page 2">2</a><a href="/3" title="Page 3">3</a><a href="/4" title="Page 4">4</a><a href="/5" title="Page 5">5</a><a href="/6" title="Page 6">6</a><a href="/7" title="Page 7">7</a><a href="/8" title="Page 8">8</a><a href="/9" title="Page 9">9</a><a href="/10" title="Page 10">10</a><a href="/11" title="Page 11">11</a><a href="/12" title="Page 12">12</a><a href="/13" title="Page 13">13</a>...<a href="/13" title="Last page">Last »</a></div>

	</div>
	<!-- Start Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div id="logo2">
		</div>
	</aside>
	<!-- End Sidebar Logos -->


	<aside class="sidebar walleft1">
		<div class="sidebarmenunavigation">
		</div>
	</aside>


	<aside class="sidebar walleft1">
		
				<div id="sidebars" class="sidebar">
					<div class="sidebar_list">
						<ul class="rand-text">						
							@foreach ($related as $rel)
								<ul class="popular-posts">
									<li><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}">{{ ucwords($rel) }}</a><div class="sidebartextviews">» {{ rand(1000,3000) }}  views</div></li>
								</ul>
							@endforeach
						</ul>
					
						<div style="clear: both"></div>
					</div>
					<div class="ads_sidebar"><?=$money['responsiveAds']; //Ads ?><!--ads--></div>
				</div>	
			</aside>
</div>
@endsection