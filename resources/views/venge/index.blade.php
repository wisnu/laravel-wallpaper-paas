@extends('main')
@section('title')
	Free Download {{ ucwords(Config::get('money.niche')) }}
@endsection

@section('meta')
	<meta name="google-site-verification" content="{{ Config::get('money.metaverification')}}" />
@endsection

@section('content')
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div id="main-container" class="col-md-9">
							<ol class="breadcrumb">
								<li><a href="{{ route('home') }}">Home</a></li>
								<li><a href="#">{{ ucwords(Config::get('money.niche')) }}</a></li>
							</ol><!--breadcrumb-->


							<div>
								<?=$money['responsiveAds']; //Ads ?>	
							</div>

							<section class="category-box box-style1 push_bottom_30 custom1-color">

								<div class="box-content row">
									@foreach ($result as $data)
									<article class="col-md-4 col-sm-4">
										<h2 class="article-title">
											<a href="{{ url('/'.str_slug($data)) }}" title="{{ ucwords(str_replace('-', ' ', $data)) }}" rel="bookmark">{{ substr(ucwords(str_replace('-', ' ', $data)), 0, 33) }}</a>
										</h2>
									</article>
									@endforeach

								</div>
							</section>






							<div class="clearfix"></div>
						</div>
						<aside class="sidebar col-md-3">
							<div class="widget widget_search push_bottom_30">
								<form role="search" method="get" action="archive.html" class="search-form">
									<div class="form-group">
										<input type="text" name="s" value="Type a keyword and hit enter ....." onfocus="if (this.value == 'Type a keyword and hit enter .....') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type a keyword and hit enter .....';}" class="form-control search-widget-input">
									</div>
								</form>
							</div>

							<div class="widget widget-social-counter push_bottom_30">

							</div>
							<div class="widget widget-tabbed push_bottom_30">
							<?=$money['responsiveAds']; //Ads ?>	
							</div>
							
							<div class="widget widget-tabbed push_bottom_30" id="widget_tabs">
								<div class="panel-group">

									<div class="tab-content">



										<div class="tab-pane box-content row active" id="recent_widget_tabs">
											<article class="article other-article side-article col-md-12">
												@foreach ($related as $rel)
												<h4 class="article-title"><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a></h4>
												@endforeach
											</article>
										</div>

@endsection