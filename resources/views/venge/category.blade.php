@extends('main')
@section('title')
	Free Download {{ $title }}
@endsection

@section('meta')
<meta name="description" content="Download {{ $title }}">
<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
@endsection


@section('content')
<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div id="main-container" class="col-md-9">
							<ol class="breadcrumb">
								<li><a href="{{ route('home') }}">Home</a></li>
								<li><a href="{{ url()->current() }}">{{ $title }}</a></li>
							</ol><!--breadcrumb-->
							
							<div>
								<?=$money['responsiveAds']; //Ads ?>	
							</div>


							<section class="category-box related-posts scroll-box push_bottom_30">

								<div class="scroll-content" id="scroll-1">
									<div class="scroll-item row">
										@foreach ($result->data as $data)
										<?php
											$tbId = explode('tbn:', $data->tbUrl);
											$tbId = $tbId[1];

											if (isset($data->gid)) {

												$imgId = substr(base64_encode($data->gid), 0, 18);
											}
											else  {
												$imgId = substr(base64_encode($data->_id), 0, 18);
											}
										?>
										<article class="scroll-article col-md-4 col-sm-4">
											<div class="article-image">
												<a href="{{ url()->current() .'/'. str_slug($data->content).'_'.$imgId }}.html" rel="bookmark">
													<h3 class="article-title">{{ $data->content }}</h3>
													<img src="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/262/140') }}" alt="{{ $data->content }}" id="exifviewer-img-7" oldsrc="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/262/140') }}">
												</a>
											</div>
										</article>
										@endforeach

									</div><!--scrool-item-->
								</div><!--scroll-content-->
							</section><!--related-box-->

						</div>
						<aside class="sidebar col-md-3">
							<div class="widget widget_search push_bottom_30">
								<form role="search" method="get" action="archive.html" class="search-form">
									<div class="form-group">
										<input type="text" name="s" value="Type a keyword and hit enter ....." onfocus="if (this.value == 'Type a keyword and hit enter .....') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type a keyword and hit enter .....';}" class="form-control search-widget-input">
									</div><!-- from group -->
								</form><!-- search form -->
							</div><!-- Search widget-->

							<div class="widget widget-tabbed push_bottom_30">
							<?=$money['responsiveAds']; //Ads ?>	
							</div>

							<div class="widget widget-tabbed push_bottom_30" id="widget_tabs">
								<div class="panel-group">
									<div class="tab-content">
										<div class="tab-pane box-content row active" id="recent_widget_tabs">
											<article class="article other-article side-article col-md-12">
												@foreach ($related as $rel)
												<h4 class="article-title"><a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a></h4>
												@endforeach
											</article>
										</div>

@endsection