<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
  	<head>
	    <title>
	    	@yield('title')
		</title>
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	    <meta name="viewport" content="width=device-width, initial-scale=1" />
	    <meta charset="UTF-8" />
		<meta content="index,follow,imageindex" name="googlebot">
		<meta content="all,index,follow" name="robots">
		<meta content="all,index,follow" name="googlebot-Image">
		<meta content="width=device-width,initial-scale=1" name="viewport">
		<meta content="general" name="rating">
		<meta content="global" name="distribution">
		<meta content="en" name="language">
		<link rel="profile" href="http://gmpg.org/xfn/11" />
	    <link rel="pingback" href="http://127.0.0.1/mega/xmlrpc.php" />
		<link rel="canonical" href="{{ url()->current() }}" />
		<link rel='stylesheet' id='bootstrap-css'  href='{{ theme_url('css/bootstrap.min.css') }}' type='text/css' media='all' />
		<link rel='stylesheet' id='font-awesome-css'  href='{{ theme_url('css/font-awesome.min.css') }}' type='text/css' media='all' />
		<link rel="stylesheet" type="text/css" media="all" href="{{ theme_url('css/main.css') }}" />
		<link rel="stylesheet" type="text/css" media="all" href="{{ theme_url('css/custom.css') }}" />
		@yield('meta')
	</head>
<body class="home blog logged-in">



		<div class="wrapper">


			<header id="header" class="push_bottom_40">
				<nav id="top-nav">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-8">
								<div id="news-ticker" style="padding-left: 138px;">
									<div id="news-ticker-title">
									    <span>{{ $_SERVER['HTTP_HOST'] }}</span><i class="fa fa-bolt"></i>
									</div>
        						</div>
							</div>

							<div class="col-md-6 col-sm-4">
								<div class="social-icons">
									<ul id="top-social-icons">
										<li class="google">
											<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>
										</li>
										<li class="youtube">
											<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Youtube"><i class="fa fa-youtube"></i></a>
										</li>
										<li class="twitter">
											<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
										</li>
										<li class="facebook">
											<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
										</li>
										<li class="rss">
											<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Rss"><i class="fa fa-rss"></i></a>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</nav>



				<nav class="main-navgation-bar container">

				</nav>
			</header>


@yield('content')
									</div><!--tab-content-->
								</div><!--panel-group-->
							</div><!--widget-tab-->




						</aside><!--sidebar-->
					</div><!--row-->
				</div><!--container-->
			</div>

			<footer id="footer">

				<div id="footer-bar">
					<div class="container">
						<div class="row">
							<div class="col-md-7 col-sm-4">
								<div id="footer-nav-bar" class="footer-navbar">
									<ul class="footer-navbar-menu">
										<li class="menu-item "><a href="{{ url('page/privacy-policy.html') }}">Privacy</a></li>
										<li class="menu-item "><a href="{{ url('page/dmca.html') }}">DMCA</a></li>
										<li class="menu-item "><a href="{{ url('page/toc.html') }}">Terms & Condition</a></li>
										<li class="menu-item"><a href="{{ url('page/contact.html') }}">Contact Us </a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-5 col-sm-8">
								<span class="copyright">Copyright {{ date('Y') }} {{ $_SERVER['HTTP_HOST'] }}</span>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<script type="text/javascript" src="{{ theme_url('js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ theme_url('js/main.js') }}"></script>
		<!-- JS files for IE -->
		<!--[if lt IE 9]>
	    	<script src="{{ theme_url('js/html5shiv.min.js') }}"></script>
	    	<script src="{{ theme_url('js/respond.min.js') }}"></script>
	    	<script src="{{ theme_url('js/selectivizr-min.js') }}"></script>
	    <![endif]-->
   <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3915070,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?{{ $money['trackid'] }}&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>
</html>