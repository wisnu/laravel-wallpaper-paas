@extends('main')
@section('title')
	{{ ucwords(Config::get('money.niche')) }}
@endsection

@section('meta')
	<meta name="google-site-verification" content="{{ Config::get('money.metaverification')}}" />
	<link href='{{ url()->current() }}' rel='canonical'>
	<meta name="description" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
	<meta name="keywords" content="{{ implode(', ', array_slice($related, 0, 10)) }}">
@endsection


@section('content')
		<div class="cl">
		</div>
	</div>


	<div id='cc'>
		<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
			<span typeof="v:Breadcrumb"><a href="/" property="v:title" rel="v:url">Home</a></span>
			» 
			<span typeof="v:Breadcrumb" class="crent"><a href="{{ url($slug) }}"></a>{{ ucwords(str_replace('-', ' ', $slug)) }}</span>
			»
			<span class="crent">{{ ucwords($item->data[0]->content) }}</span>
		</div>


	<div id="cl">
		<div class="content">
			<h1 class="ld">{{ ucwords($item->data[0]->content) }}</h1>
	
	
			<div class="ads-top">
				//Ads
			</div>
	
	
			<figure>
				<img alt="{{ ucwords($item->data[0]->content) }}" class="attachment-full wallpaper" exifid="-331487644" id="exifviewer-img-0" oldsrc="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}" onerror="this.src='{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}'" src="{{ url('image/'.$id.'/'.str_slug($item->data[0]->content)) }}">
	
				<figcaption>
					{{ ucwords($item->data[0]->content) }}
				</figcaption>
			</figure>
	
	
			<div class="ads-top">
				// Ads
			</div>

			@foreach ($result->data as $data)
			<?php
				$tbId = explode('tbn:', $data->tbUrl);
				$tbId = $tbId[1];
				
				$imgId = explode('.', explode('/', $data->url)[4]);
				$imgId = $imgId[0];

			?>
				<div class="box">
					<a class="th" href="{{  str_slug($data->content).'_'.$imgId }}.html" title="{{ $data->content }}"><img alt="{{ $data->content }}" exifid="-331487644" height="100" id="exifviewer-img-0" oldsrc="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/225/140') }}" onerror="null;this.src='{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/225/140') }}'" src="{{ url('thumbnail/'.$tbId.'/'.str_slug($data->content).'/225/140') }}" width="225"></a>

					<h2>{{ $data->content }}</h2>
				</div>


			@endforeach
	
	
	
		
			<div style="clear:both">
			</div>
	
	
			<div style="clear:both">
			</div>
	
	
			<h3 class="ld">Related {{ ucwords($item->data[0]->content) }}</h3>
			<ul>
				@foreach ($related as $rel)
				<li>
					<a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a>
				</li>

				@endforeach
			</ul>
		</div>
	</div>

		<div id="sb">
			<div class="cl">
			</div>


			<h3 class="hc">Random post:</h3>


			<ul class="rand-text">
				@foreach ($related as $rel)
				<li>
					<h3>
					<a href="{{ url(str_slug($rel)) }}" title="{{ ucwords($rel) }}" rel="bookmark">{{ ucwords($rel) }}</a>
					</h3>
				</li>

				@endforeach

			</ul>


			<div class="cl">
			</div>
		</div>


		<div class="cl">
		</div>

	</div>
@endsection